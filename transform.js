fs = require("fs");
var parser = require("xml2json");

fs.readFile("./input.xml", function(err, data) {
  const transactions = JSON.parse(parser.toJson(data))["Statement"][
    "Transactions"
  ]["Transaction"];

  let csv = "Date,Payee,Inflow\n";

  transactions.forEach(t => {
    const fdate = t["TransactionDate"].split(" ")[0].split("-");
    csv += `${fdate[2]}/${fdate[1]}/${fdate[0]},${t["Details"]},${
      t["TransactionSum"].replace(",", ".").split(".")[0]
    }.00\n`;
  });

  fs.writeFileSync("./result.csv", csv);
});
